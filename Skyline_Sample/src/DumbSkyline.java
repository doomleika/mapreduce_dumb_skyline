import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class DumbSkyline {
	List<SkylineEntry> list = new ArrayList<SkylineEntry>();
	
	DumbSkyline insert(SkylineEntry entry) {

		Iterator<SkylineEntry> iter = list.iterator();
		
		// Check if being dominated
		while(iter.hasNext()) {
			SkylineEntry in_list = iter.next();
			if(in_list.dominate(entry)) {
				// being dominated, bail out
				return this;
			}
		}
		// Check if dominate anyone in list
		iter = list.iterator();

		while(iter.hasNext()) {
			SkylineEntry in_list = iter.next();
			if(entry.dominate(in_list)) {
				iter.remove();
			}
		}
		
		list.add(entry);
	
		return this;
	}
	
	Iterator<SkylineEntry> getIter() {
		return this.list.iterator();
	}
}
