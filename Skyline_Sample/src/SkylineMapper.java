import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;


public class SkylineMapper extends Mapper<LongWritable, Text, Text, Text>{
	DumbSkyline ds = new DumbSkyline();
	
	@Override
	protected void map(LongWritable key, Text value, Context context)
		throws IOException, InterruptedException
	{
		int[] ints = Utility.convertStringToInts(value.toString());
		SkylineEntry entry = new SkylineEntry(ints);
		this.ds.insert(entry);
	}
	
	@Override
	protected void cleanup(Context context)	
			throws IOException, InterruptedException
	{
		Iterator<SkylineEntry> iter = this.ds.getIter();
		while(iter.hasNext()) {
			SkylineEntry entry = iter.next();

			context.write(new Text(""), new Text(entry.toString()));
			//Key does not matter
		}
	}
}
