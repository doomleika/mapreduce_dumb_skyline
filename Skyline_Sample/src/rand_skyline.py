import random

NODES = 2000000
RANGE = 50000
FILE_NAME = "sky_nodes.txt"

with open(FILE_NAME, 'w') as file_handle:
    for x in range(NODES):
        file_handle.write(str(random.randrange(RANGE)) +
        ' ' + str(random.randrange(RANGE)) +
        ' ' + str(random.randrange(RANGE)) +
        ' ' + str(random.randrange(RANGE)) +
        "\n")
    

