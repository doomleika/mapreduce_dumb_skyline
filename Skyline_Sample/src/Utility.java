public class Utility {

	static public int[] convertStringToInts(String str) {
		String [] strs = str.split(" ");
		int[] results = new int[strs.length];
		
		for(int x=0; x<strs.length; ++x) {
			results[x]	= Integer.parseInt(strs[x]);
		}
		return results;
	}

}
