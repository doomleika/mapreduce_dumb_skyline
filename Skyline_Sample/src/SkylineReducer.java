import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;


public class SkylineReducer extends Reducer<Text, Text, Text, Text>{
	DumbSkyline ds = new DumbSkyline();
	
	@Override
	protected void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException
	{
		for(Text value: values) {
			int[] ints = Utility.convertStringToInts(value.toString());
			SkylineEntry entry = new SkylineEntry(ints);
			this.ds.insert(entry);
		}
	}
	
	@Override
	protected void cleanup(Context context)
			throws IOException, InterruptedException
	{
		Iterator<SkylineEntry> iter = this.ds.getIter();
		while(iter.hasNext()) {
			SkylineEntry entry = iter.next();

			context.write(new Text(""), new Text(entry.toString()));
			//Key does not matter
		}
	}
	
}
