import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class SkylineDriver extends Configured implements Tool{
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new SkylineDriver(), args);
		System.exit(res);
	}
	
	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = this.getConf();
		Job job = new Job(conf);
		job.setJarByClass(SkylineDriver.class);
		
		// Set your program arguments here
		if(args.length != 2) {
			System.out.println("<input> <output>");
			System.exit(1);
		}
		job.setJobName("DumbSkyline " + args[0] + " " + args[1]);

		

		FileInputFormat.setInputPaths(job, new Path(args[0]));
        job.setInputFormatClass(TextInputFormat.class);
		
		
		job.setMapperClass(SkylineMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);	
		
		job.setNumReduceTasks(1);
		
		job.setPartitionerClass(SkylinePartitioner.class);
		
		job.setReducerClass(SkylineReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputFormatClass(TextOutputFormat.class);
		
		return job.waitForCompletion(true) ? 0 : 1;
	}

}
