
public class SkylineEntry {
	int[] items = null;
	
	public SkylineEntry(int[] ints) {
		// fool proof
		assert ints.length == 4;
		this.items = ints;
	}
	
	int[] getEntries() {
		return this.items;
	}
	
	public boolean dominate(SkylineEntry entry) {
		if(this.items[0] == entry.items[0] &&
			this.items[1] == entry.items[1] &&
			this.items[2] == entry.items[2] &&
			this.items[3] == entry.items[3])
		{
			// Not dominate, all equal
			return false;
		} else if (this.items[0] >= entry.items[0] &&
			this.items[1] >= entry.items[1] &&
			this.items[2] >= entry.items[2] &&
			this.items[3] >= entry.items[3])
		{
			// Dominate, all equal or at least better
			return true;
		} else {
			// Not dominate
			return false;
		}
	}
	
	@Override
	public String toString() {
		String result = "";
		result += this.items[0] + " ";
		result += this.items[1] + " ";
		result += this.items[2] + " ";
		result += this.items[3];
		
		return result;
	}
	
	
}
